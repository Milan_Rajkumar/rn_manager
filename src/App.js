import React , { Component } from 'react';
import { View, Text } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import { NativeRouter, Route, Link } from 'react-router-native';
import LoginForm from './components/LoginForm';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';

const config = {
  apiKey: 'AIzaSyBVvtJr91rmGk3M6jA6e16Yv14yg2ABP6M',
  authDomain: 'manager-2f1d7.firebaseapp.com',
  databaseURL: 'https://manager-2f1d7.firebaseio.com',
  projectId: 'manager-2f1d7',
  storageBucket: 'manager-2f1d7.appspot.com',
  messagingSenderId: '316000219883'
};
firebase.initializeApp(config);

class App extends Component {
  render () {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <NativeRouter>
        <Provider store = { store }>
          <View>
            <Route exact path="/" component={LoginForm}/>
            <Route exact path="/employeeCreate" component={EmployeeCreate}/>
            <Route exact path="/employeeList" component={EmployeeList}/>
          </View>
        </Provider>
      </NativeRouter>
    )
  }
}

export default App;
