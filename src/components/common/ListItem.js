import React, {Component} from 'react';
import {CardSection} from './common';
import { Text, TouchableWithoutFeedback, View } from 'react-native';

class ListItem extends Component {
  onRowPress() {
    // go to employee create ui
  }
  render () {
    const { name } = this.props.employee;
    return(
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View>
          <CardSection>
            <Text style = {styles.titleStyle}>
            {name}
            </Text>
          </CardSection>
        </View>
      </TouchableWithoutFeedback>
    );
    )
  }
}



const styles = {
  titleStyle: {
    fontSize: 18,
    paddingLeft: 15
  }
};
