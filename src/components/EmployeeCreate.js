import React, { Component } from 'react';
import { Card, CardSection, Input, Button } from './common';
import {employeeUpdate, employeeCreate} from '../actions';
import { connect } from 'react-redux';
import { Picker, Text } from 'react-native';

class EmployeeCreate extends Component {
  createEmployee () {
    const { name, phone, shift, history } = this.props;
    console.log(history)
    this.props.employeeCreate({ name, phone, shift: shift || 'Monday', history })
  }
  render () {
    return (
      <Card>
        <CardSection>
          <Input
            label = "Name"
            placeholder = "Jane"
            value = {this.props.name}
            onChangeText = {value => this.props.employeeUpdate({ prop: 'name', value })}/>
        </CardSection>

        <CardSection>
          <Input
            label = "Phone"
            placeholder = "555-555-555"
            value = {this.props.phone}
            onChangeText = {value => this.props.employeeUpdate({ prop: 'phone', value })}/>
        </CardSection>

        <CardSection style = {{ flexDirection: 'column' }}>
          <Text style = {styles.pickerLabelStyle}>Shift</Text>
          <Picker
            style = {styles.pickerStyle}
            selectedValue = { this.props.shift }
            onValueChange = { value => this.props.employeeUpdate({ prop: 'shift', value })}>
            <Picker.Item label = "Monday" value = "Monday"/>
            <Picker.Item label = "Tuesday" value = "Tuesday"/>
            <Picker.Item label = "Wednesday" value = "Wednesday"/>
            <Picker.Item label = "Thursday" value = "Thursday"/>
            <Picker.Item label = "Friday" value = "Friday"/>
            <Picker.Item label = "Saturday" value = "Saturday"/>
            <Picker.Item label = "Sunday" value = "Sunday"/>
          </Picker>
        </CardSection>

        <CardSection>
          <Button onPress = { this.createEmployee.bind(this)}>Create</Button>
        </CardSection>
      </Card>
    )
  }
}
const mapStateToProps = ({employeeForm}) => {
  const { name, phone, shift, history } = employeeForm;
  return { name, phone, shift, history }
};

const styles = {
  pickerStyle: {
    flex: 1
  },
  pickerLabelStyle: {
    flex: 1,
    paddingLeft: 20
  }
}

export default connect(mapStateToProps, {employeeUpdate, employeeCreate}) (EmployeeCreate);
