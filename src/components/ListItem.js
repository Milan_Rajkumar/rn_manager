import React, {Component} from 'react';
import {CardSection} from './common';
import { Text } from 'react-native';

class ListItem extends Component {
  render () {
    const { name } = this.props.employeeUpdate
    return(
      <CardSection>
        <Text style= {styles.styles}>{ name }</Text>
      </CardSection>
    )
  }
}
const mapStateToProps = (state, ownProps) => {
  const expanded = state.selectedLibraryId === ownProps.library.id;
  return {expanded}
};


const styles = {
  titleStyle: {
    fontSize: 18,
    paddingLeft: 15
  }
};

export default connect(mapStateToProps, actions) (ListItem);
