import React, { Component } from 'react';
import { ListView } from 'react-native';
import {Toolbar} from './common';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { employeesFetch } from '../actions';
import ListItem from './ListItem'

class EmployeeList extends Component {
  componentWillMount() {
    this.props.employeesFetch();
    this.createDataSource(this.props)
  }
  componentWillReceiveProps(nextProps) {
    this.createDataSource(this.nextProps)
  }
  createDataSource({employess}) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(employess)
  }
  onActionSelected (history, position) {
    if (position === 0) {
      history.push("/employeeCreate")
    }
  }
  renderRow(employee) {
    return <ListItem employee = {employee}/>
  }
  render () {
    const { history } = this.props;
    return (
      <View>
        <Toolbar onActionSelected = { this.onActionSelected.bind(this, history) }/>
        <ListView
          enableEmptySections
          dataSource = { this.dataSource }
          renderRow = { this.renderRow }
      </View>
    )
  }
}
const mapStateToProps = state => {
  const employees = map(state.employees, (val, uid)) => {
    return { ...val, uid }
  }
  return employees;
}
export default connect(mapStateToProps, {employeesFetch})(EmployeeList);
