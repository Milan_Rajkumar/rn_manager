import React, { Component } from 'react';
import { Card, CardSection, Input, Button } from './common';

class LoginForm extends Component {
  state = { email: '', password: ''}
  render () {
    return (
      <Card>
        <CardSection>
          <Input
            label = "Email"
            value = { this.state.email }
            placeholder = "user@gmail.com"
            onChangeText = { email => this.setState({ email })}/>
        </CardSection>
        <CardSection>
          <Input
            secureTextEntry = { true }
            label = "Password"
            placeholder = "password"
            value = { this.state.password }
            onChangeText = { password => this.setPassword({ password })}/>
        </CardSection>
        <CardSection>
          <Button>
            Login
          </Button>
        </CardSection>
      </Card>
    )
  }
}

export default LoginForm;
