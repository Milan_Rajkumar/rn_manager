import { EMAIL_CHANGED, PASSWORD_CHANGED, LOGIN_USER_SUCCESS, LOGIN_USER_FAIL, LOGIN_USER } from './types';
import firebase from 'firebase';

export const emailChanged = ( text ) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  }
}

export const passwordChanged = ( text ) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  }
}

const loginUserSuccess  = (dispatch, user, history) => {
  dispatch({ type: LOGIN_USER_SUCCESS, payload: user });
  history.replace("/employeeList")
}

const loginUserFail = (dispatch) => {
  dispatch({ type: LOGIN_USER_FAIL });
}

export const loginUser = (email, password, history) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER })
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => loginUserSuccess(dispatch, user, history))
      .catch (() => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
          .then (user => loginUserSuccess(dispatch))
          .catch (() => { loginUserFail(dispatch);
          });
      })
  }
}
