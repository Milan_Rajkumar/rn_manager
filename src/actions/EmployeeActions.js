import { EMPLOYEE_UPDATE, EMPLOYEE_CREATE, EMPLOYEES_FETCH_SUCCESS  } from './types';
import firebase from 'firebase';

export const employeeUpdate = ({ prop, value }) => {
  return {
    type: EMPLOYEE_UPDATE,
    payload: { prop, value }
  }
};

export const employeeCreate = ({name, phone, shift, history}) => {
  console.log(name, phone, shift, history)
  const {currentUser} = firebase.auth()
  console.log('firebase.auth().currentUser-------------', currentUser)
  return (dispatch) => {
    firebase.database().ref(`users/${currentUser}/employees`).push({name, phone, shift})
    .then(() => {
      // history.push("/employeeList")
      dispatch({ type: EMPLOYEE_CREATE })
      console.log('sucessfully inserted to firebase database');
    })
  }
}

export const employeesFetch= () => {
  return (dispatch) => {
    const {currentUser} = firebase.auth()
    firebase.database().ref(`/users/${currentUser.uid}/employees`)
    .on('value', snapshot => {
      dispatch({ type: EMPLOYEES_FETCH_SUCCESS, payload: snapshot.val()})
    });
  }
}
