import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import EmployeeFormReducers from './EmployeeFormReducers';
import EmployeesReducers from '/EmployeesReducers';

export default combineReducers({
  auth: AuthReducer,
  employeeForm: EmployeeFormReducers
  employeesReducers: EmployeesReducers
});
